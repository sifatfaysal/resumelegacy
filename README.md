# Dev Resume

Welcome to the **Dev Resume** repository! This project is an open-source web application built using React, Next.js, TypeScript, React Three.js, Tailwind CSS, and several other technologies. It leverages Strapi as the backend with Node.js and MySQL, along with GraphQL and Storybook for additional functionality. This README.md file will guide you through the project structure and provide instructions on how to contribute.

## Folder Structure

The project follows a well-organized folder structure for better code management and maintainability. Here's an overview of the main folders and their purposes:



```
├── example.json
├── index.html
├── index.js
├── package.json
├── package-lock.json
├── README.md
└── src
    ├── app.js
    ├── models.js
    ├── routes.js
    └── utils
        ├── another.js
        ├── constants.js
        └── index.js
```




## Getting Started

To get started with the project, follow the steps below:

1.  Clone the repository to your local machine using the following command:
 
    `git clone https://github.com/sifatfaysal/dev-resume` 
    
2.  Navigate to the project's root directory:
    
    `cd dev-resume` 
    
3.  Install the dependencies using your package manager of choice. For example, with npm:
    
    `npm install` 
    
4.  Configure the backend by setting up Strapi and MySQL. Refer to the project documentation for detailed instructions on configuring and running the backend.
    
5.  Start the development server:

    `npm run dev` 
    
6.  Access the application by opening your browser and visiting [http://localhost:5500](http://localhost:5500/).


## Docker
You can also run the Dev Resume application using Docker. Here's how:

1. Make sure you have Docker installed on your machine.

2. Pull the Docker image from the repository:

`docker pull sifatfaysal/dev-resume`

3. Run the Docker container:

`docker run -p 5500:5500 sifatfaysal/dev-resume`

4. Access the application by opening your browser and visiting http://localhost:5500.

## Contributing

We welcome contributions from the open-source community to help improve this project. If you would like to contribute, please follow these steps:

1.  Fork the repository to your GitHub account.
    
2.  Clone your forked repository to your local machine.
    
3.  Create a new branch for your feature or bug fix:
    
    `git checkout -b feature/your-feature-name` 
    
4.  Make the necessary changes and additions.
    
5.  Commit your changes:
    
    `git commit -m "Add your commit message here"` 
    
6.  Push your branch to GitHub:
    
    `git push origin feature/your-feature-name` 
    
7.  Open a pull request from your branch to the main repository's `main` branch.
    
8.  Wait for the maintainers to review your pull request. Address any feedback or comments if necessary.
    

Please ensure that your contributions adhere to the project's code style guidelines and maintain a high standard of quality.

## License

The **Project Name** project is licensed under the [MIT License](https://github.com/). Please review the license file for more information.

## Contact

If you have any questions or need further assistance, feel free to reach out to the project maintainers or contributors via the provided contact information in the repository.

Thank you for your interest in contributing to the **Dev Resume** project! We appreciate your support and look forward to your contributions.
