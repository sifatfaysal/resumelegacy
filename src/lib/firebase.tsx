import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyBh4YywP3RXeWNUYSELHIx_njxaD1wBB60",
  authDomain: "sf14web.firebaseapp.com",
  projectId: "sf14web",
  storageBucket: "sf14web.appspot.com",
  messagingSenderId: "177081282061",
  appId: "1:177081282061:web:8109374cf60b983c81ef44",
  measurementId: "G-J99GMP8NLJ",
};

const app = initializeApp(firebaseConfig);
const auth = getAuth(app);

export default auth;

