import React, { useRef, useState } from "react";
import { RiSearch2Line } from "react-icons/ri";
import { useOutsideClickHandler } from "@/hooks";
import classNames from "classnames";
import blogPosts from "@/data/blogPosts";
import { useRouter } from "next/router";

interface Props {
  placeholder?: string;
}

const SearchField: React.FC<Props> = ({ placeholder = "Search Post" }) => {
  const ref = useRef<any>(null);
  const router = useRouter();
  const [isOpen, setIsOpen] = useState(false);
  const [value, setValue] = useState("");

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setValue(e.target.value);
  };

  const filteredPosts = blogPosts?.post?.filter((post) =>
    post?.title?.toLowerCase()?.includes(value?.toLowerCase()),
  );
  const suggestionsOpen =
    isOpen && value?.trim() !== "" && filteredPosts?.length > 0;

  const handleClickSuggestionItem = (item: any) => {
    router?.push(`/blog/${item?.slug}`);
  };

  useOutsideClickHandler(ref, () => setIsOpen(false));

  return (
    <div className="relative" ref={ref}>
      <input
        type="text"
        value={value}
        onChange={handleInputChange}
        placeholder={placeholder}
        className="w-[300px] py-2 pl-10 pr-4 text-gray-700 bg-graphite placeholder-gray-500 border border-ocean rounded-md shadow-sm focus:outline-none focus:ring-2 focus:ring-ocean focus:border-ocean sm:text-sm"
        onClick={() => setIsOpen(true)}
      />
      <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
        <RiSearch2Line className="w-5 h-5 text-gray-400" aria-hidden="true" />
      </div>

      <div
        className={classNames(
          "absolute left-0 right-0 top-[100%] bg-[#1f2a38] rounded-[5px] p-[5px] z-[1] shadow-lg",
          {
            hidden: !suggestionsOpen,
            block: suggestionsOpen,
          },
        )}
      >
        {filteredPosts?.map((item, index) => (
          <div
            key={index}
            className={"p-[10px] hover:bg-ocean cursor-pointer rounded-[5px]"}
            onClick={() => handleClickSuggestionItem(item)}
          >
            {item?.title}
          </div>
        ))}
      </div>
    </div>
  );
};

export default SearchField;