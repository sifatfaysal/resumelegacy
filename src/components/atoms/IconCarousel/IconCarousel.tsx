import React, { useEffect, useState } from "react";
import {
  SiJavascript,
  SiPcgamingwiki,
  SiPython,
  SiReact,
  SiSass,
  SiVisualstudiocode,
} from "react-icons/si";
import { FaAws } from "react-icons/fa";

const IconCarousel = () => {
  const [currentIcon, setCurrentIcon] = useState(0);
  const icons = [
    <SiJavascript
      key="js"
      className="w-[40px] h-[40px] sm:w-[30px] sm:h-[30px] md:w-[40px] md:h-[40px] text-[#F7DF1E]"
    />,
    <SiReact
      key="js"
      className="w-[40px] h-[40px] sm:w-[30px] sm:h-[30px] md:w-[40px] md:h-[40px] text-[#00D8FF]"
    />,
    <SiPcgamingwiki
      key="js"
      className="w-[40px] h-[40px] sm:w-[30px] sm:h-[30px] md:w-[40px] md:h-[40px] text-[#cc6699]"
    />,
    <SiVisualstudiocode
      key="ide"
      className="w-[40px] h-[40px] sm:w-[30px] sm:h-[30px] md:w-[40px] md:h-[40px] text-[#0078d7]"
    />,
  ];

  useEffect(() => {
    if (typeof window !== "undefined") {
      const interval = setInterval(() => {
        setCurrentIcon((prev) => (prev + 1) % icons.length);
      }, 3000);

      return () => clearInterval(interval);
    }
  }, [icons.length]);

  return (
    <div className="icon-carousel">
      {icons.map((icon, index) => (
        <div
          key={index}
          className={`icon-container ${index === currentIcon ? "active" : ""}`}
          onClick={() => setCurrentIcon(index)}
        >
          {icon}
        </div>
      ))}
    </div>
  );
};

export default IconCarousel;