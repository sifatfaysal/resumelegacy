import React, { useState, useEffect } from "react";
import { GiCookie } from "react-icons/gi";

const CookieConsent: React.FC = () => {
  const [showBanner, setShowBanner] = useState<boolean>(false);

  const handleAccept = () => {
    setShowBanner(false);
    localStorage.setItem("cookieConsent", "accepted");
  };

  const handleReject = () => {
    setShowBanner(false);
    localStorage.setItem("cookieConsent", "rejected");
  };

  useEffect(() => {
    const cookieConsentStatus = localStorage.getItem("cookieConsent");
    if (!cookieConsentStatus) {
      setShowBanner(true);
    }
  }, []);

  if (!showBanner) return null;

  return (
    <div className="bg-[#2C333C] border border-[#3e3d3d] w-[600px] fixed bottom-4 left-4 right-0 p-4 bg-gray-800 text-white flex flex-col items-center justify-center rounded-xl">
      <div className="flex gap-5 items-center justify-center">
        <GiCookie className="" size={70} color="white" />
        <div className="">
          We use third-party cookies in order to personalize your site
          experience.❤️
        </div>
        <div className="flex gap-2 justify-center">
          <button
            onClick={handleAccept}
            className="px-4 py-2 crystal text-white"
          >
            Allow
          </button>
          <button
            onClick={handleReject}
            className="px-4 py-2 crystal text-white"
          >
            Decline
          </button>
        </div>
      </div>
    </div>
  );
};

export default CookieConsent;