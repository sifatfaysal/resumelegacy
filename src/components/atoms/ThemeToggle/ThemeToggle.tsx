import { useTheme } from "@/context/ThemeContext";

const DarkModeToggle = () => {
  const { darkMode, toggleDarkMode } = useTheme();

  const handleToggleClick = () => {
    console.log("Before toggling - darkMode:", darkMode);
    toggleDarkMode();
  };

  console.log("Current mode:", darkMode);

  return (
    <button
      onClick={handleToggleClick}
      className="px-4 py-2 rounded-md  text-white"
    >
      {darkMode ? "🌞" : "🌙"}
    </button>
  );
};

export default DarkModeToggle;