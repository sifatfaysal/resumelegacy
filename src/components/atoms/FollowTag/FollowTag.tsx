import React from "react";
import { BsLinkedin } from "react-icons/bs";
import { SiGithubsponsors } from "react-icons/si";
import { FiGithub } from "react-icons/fi";
import Button from "@/components/atoms/Button/Button";
import NextLink from "next/link";

const handleButtonClick = () => {
  console.log("Button clicked!");
};

export default function FollowTag() {
  return (
    <>
      <div className="flex flex-col gap-5 cursor-pointer h-48 lg:h-full items-center">
        <div className="flex flex-col items-center justify-center gap-[20px]">
          <NextLink href="https://www.linkedin.com/in/sifatfaysal/" passHref>
            <BsLinkedin className="cursor-pointer hover:text-ocean" />
          </NextLink>
          <NextLink href="https://www.buymeacoffee.com/sifat" passHref>
            <SiGithubsponsors className="cursor-pointer hover:text-ocean" />
          </NextLink>
          <NextLink href="https://github.com/sifatfaysal" passHref>
            <FiGithub className="cursor-pointer hover:text-ocean" />
          </NextLink>
        </div>
        <p className="border border-white h-[90px]"></p>
        <div className="flex items-center justify-center">
          <div className="mt-[20px]">
            <NextLink href="https://www.facebook.com/sifatfaysall/" passHref>
              <Button
                className="rotate-90 bg-graphite"
                variant="outline"
                onClick={handleButtonClick}
              >
                Follow Me
              </Button>
            </NextLink>
          </div>
        </div>
      </div>
    </>
  );
}
