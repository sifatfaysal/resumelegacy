import React from "react";

interface ButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  variant?: "primary" | "outline";
  onClick?: () => void;
  children: React.ReactNode;
  className?: string;
}

const Button: React.FC<ButtonProps> = ({
  variant = "primary",
  onClick,
  children,
  className,
  ...restProps
}) => {
  const primaryClasses = "bg-ocean text-white";
  const outlineClasses = "bg-transparent border border-ocean";

  const classes = `${className || ""} ${
    variant === "primary" ? primaryClasses : outlineClasses
  }`;

  return (
    <button
      className={`px-4 py-2 rounded-md ${classes}`}
      onClick={onClick}
      {...restProps}
    >
      {children}
    </button>
  );
};

export default Button;
export type { ButtonProps };