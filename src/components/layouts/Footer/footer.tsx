import React from "react";
import ScrollToTop from "@/components/molecules/ScrollToTop/ScrollToTop";
import { SiGithubsponsors } from "react-icons/si";
import { TbBrandGithub } from "react-icons/tb";
import { BsFacebook, BsLinkedin } from "react-icons/bs";

export default function Footer() {
  return (
    <div className="bg-[#242A34] p-8 md:p-12 mt-20 md:mt-32">
      <div className="container flex flex-col md:flex-row justify-between items-center">
        <div className="flex gap-2 md:gap-2 justify-center items-center mb-4 md:mb-0">
          <div className="crystal p-[10px]">
            <a
              href="https://www.buymeacoffee.com/sifatfaysall"
              target="_blank"
              rel="noopener noreferrer"
            >
              <SiGithubsponsors className="cursor-pointer hover:text-ocean" />
            </a>
          </div>
          <div className="crystal p-[10px]">
            <a
              href="https://github.com/sifatfaysal"
              target="_blank"
              rel="noopener noreferrer"
            >
              <TbBrandGithub className="cursor-pointer hover:text-ocean" />
            </a>
          </div>
          <div className="crystal p-[10px]">
            <a
              href="https://www.facebook.com/sifatfaysall/"
              target="_blank"
              rel="noopener noreferrer"
            >
              <BsFacebook className="cursor-pointer hover:text-ocean" />
            </a>
          </div>
          <div className="crystal p-[10px]">
            <a
              href="https://www.linkedin.com/in/sifatfaysal/"
              target="_blank"
              rel="noopener noreferrer"
            >
              <BsLinkedin className="cursor-pointer hover:text-ocean" />
            </a>
          </div>
        </div>
        <ScrollToTop />
      </div>
    </div>
  );
}