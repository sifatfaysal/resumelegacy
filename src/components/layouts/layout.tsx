import React, { ReactNode, useState } from "react";
import Navbar from "./Navbar/navbar";
import Footer from "./Footer/footer";

interface LayoutProps {
  children: ReactNode;
}

const Layout: React.FC<LayoutProps> = ({ children }) => {
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  function toggleMenu() {
    setIsMenuOpen(!isMenuOpen);
  }

  return (
    <div>
      <Navbar isMenuOpen={isMenuOpen} onToggleMenu={toggleMenu} />
      <main>{children}</main>
      <Footer />
    </div>
  );
};

export default Layout;