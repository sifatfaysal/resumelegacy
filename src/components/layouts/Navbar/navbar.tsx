import React from "react";
import Link from "next/link";
import { CgMenuGridR } from "react-icons/cg";
import { RxCross2, RxCrossCircled } from "react-icons/rx";
import { BsPeople } from "react-icons/bs";
import { BiEnvelopeOpen, BiNote } from "react-icons/bi";
import { MdOutlineMouse } from "react-icons/md";
import Button from "@/components/atoms/Button/Button";
import DarkModeToggle from "@/components/atoms/ThemeToggle/ThemeToggle";

interface MenuItem {
  label: string;
  link: string;
  sectionId: string;
  hasSectionId: boolean;
  icon: React.ReactElement;
}

interface HeaderProps {
  isMenuOpen: boolean;
  onToggleMenu: () => void;
}

const menuItems = [
  {
    label: "About",
    link: "/",
    sectionId: "about",
    hasSectionId: true,
    icon: (
      <BsPeople className="inline-block mr-1 h-5 w-5 text-white md:hidden" />
    ),
  },
  {
    label: "Experience",
    link: "/",
    sectionId: "experiance",
    hasSectionId: true,
    icon: <BiNote className="md:hidden inline-block mr-1 h-5 w-5 text-white" />,
  },
  {
    label: "Work",
    link: "/",
    sectionId: "noteworthyProjects",
    hasSectionId: true,
    icon: (
      <MdOutlineMouse className="md:hidden inline-block mr-1 h-5 w-5 text-white" />
    ),
  },
  {
    label: "Article",
    link: "/blog",
    sectionId: "",
    hasSectionId: false,
    icon: (
      <MdOutlineMouse className="md:hidden inline-block mr-1 h-5 w-5 text-white" />
    ),
  },
  {
    label: "Contact",
    link: "/",
    sectionId: "contact",
    hasSectionId: true,
    icon: (
      <BiEnvelopeOpen className="md:hidden inline-block mr-1 h-5 w-5 text-white" />
    ),
  },
];


const Header: React.FC<HeaderProps> = ({ isMenuOpen, onToggleMenu }) => {
  return (
    <header className="">
      <div className="container">
        <div className="flex items-center justify-between h-16 crystal p-[40px] rounded-md mt-[30px]">
          <div className="flex-shrink-0">
            <Link href="/">
              <span className="text-white font-bold text-xl">
                Zahid <span className="text-ocean font">.</span>
              </span>
            </Link>
          </div>
          <div className="hidden md:block">
            <nav className="flex space-x-4 justify-center items-center">
              {menuItems.map((item, index) => (
                <Link
                  href={`${item.link}${item.hasSectionId ? `#${item.sectionId}` : ""}`}
                  key={index}
                >
                  <span className="text-white hover:bg-gray-700 hover:text-ocean flex items-center px-3 py-2 rounded-md text-sm font-medium">
                    {item.icon}
                    <span className="hidden md:inline">{item.label}</span>
                  </span>
                </Link>
              ))}
              <Button variant="outline">
                <Link href="/login">Sign In</Link>
              </Button>
              <DarkModeToggle />
            </nav>
          </div>

          <div className="-mr-2 flex md:hidden">
            <Button
              type="button"
              onClick={onToggleMenu}
              className="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white"
              aria-controls="mobile-menu"
              aria-expanded={isMenuOpen}
            >
              <span className="sr-only">Open main menu</span>
              {isMenuOpen ? <RxCross2 /> : <CgMenuGridR />}
            </Button>
          </div>
        </div>
      </div>

      <div
        className={`fixed bottom-0 left-0 z-[1200] w-full transform transition-transform duration-500 ease-in-out ${
          isMenuOpen ? "translate-y-0" : "translate-y-full"
        } md:hidden bg-mint`}
        id="mobile-menu"
      >
        <div className="px-2 pt-2 pb-3 sm:px-3 bg-ocean flex justify-between items-center">
          <div className="flex flex-wrap justify-center">
            {menuItems.map((item, index) => (
              <Link
                href={`${item.link}${item.hasSectionId ? `#${item.sectionId}` : ""}`}
                key={index}
              >
                <span className="text-gray-300 hover:bg-gray-900 hover:text-white flex items-center px-3 py-2 rounded-md text-base font-medium">
                  {item.icon}
                  <span>{item.label}</span>
                </span>
              </Link>
            ))}
          </div>
          <button
            type="button"
            onClick={onToggleMenu}
            className="text-gray-300 hover:bg-gray-900 hover:text-white flex items-center px-3 py-2 rounded-md text-base font-medium"
          >
            <span className="sr-only">Close menu</span>
            <RxCrossCircled />
          </button>
        </div>
      </div>
    </header>
  );
};

export default Header;
