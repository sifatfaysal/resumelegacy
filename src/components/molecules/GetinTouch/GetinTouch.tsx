import React from "react";
import Button from "@/components/atoms/Button/Button";
import Earth from "@/components/canvas/Earth";
const handleButtonClick = () => {
  alert("Button clicked!");
};

const GetInTouch = () => {
  return (
    <div
      className="container pt-10 lg:pt-20"
      id="contact"
      data-aos="fade-down"
      data-aos-easing="linear"
      data-aos-duration="1500"
    >
      <Earth/>
      <div className="flex justify-center items-center">
        <div className="py-10">
          <p className="text-[16px] md:text-[16px] text-center leading-6  text-ocean">
            Get in touch!
          </p>
          <p className="text-[36px] md:text-[70px] font-bold text-center">
            Contact <span className="text-ocean text-[70px]">.</span>
          </p>
          <p className="text-[16px] md:text-[16px] text-center leading-6 text-[#CCBEBE] py-[10px]">
            Be the first to get exclusive offers
            <br /> and the latest news
          </p>
        </div>
      </div>

      <div className="flex justify-center">
        <div className="crystal flex flex-wrap justify-center gap-8 p-[60px] w-[900px]">
          <div className="text-start">
            <p className="text-[26px] lg:text-[26px] font-bold">Email</p>
            <p className="text-[16px] lg:text-[16px] mt-2">
              zahid.showravbd@gmail.com
            </p>
          </div>
          <div className="border-r-4 border-ocean h-16 my-auto"></div>
          <div className="text-start">
            <p className="text-[26px] lg:text-[26px] font-bold">Skype</p>
            <p className="text-[16px] lg:text-[16px] mt-2">live:zahid.showravbd</p>
          </div>
          <div className="border-r-4 border-ocean h-16 my-auto"></div>
          <div className="text-start">
            <p className="text-[26px] lg:text-[26px] font-bold">Address</p>
            <p className="text-[16px] lg:text-[16px] mt-2">
              Mirpur, Dhaka, Bangladesh
            </p>
          </div>
        </div>
      </div>

      <div className="flex justify-center items-center mt-10 lg:mt-20">
        <Button
          className="border-dashed"
          variant="outline"
          onClick={handleButtonClick}
        >
          Get in touch
        </Button>
      </div>
    </div>
  );
};

export default GetInTouch;
