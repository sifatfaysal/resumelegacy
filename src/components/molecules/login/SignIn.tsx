import React, { useState } from "react";
import Image from "next/image";
import PlaceholderImage1 from "@/static/images/m1.gif";
import PlaceholderImage2 from "@/static/images/m2.gif";
import { signInWithEmailAndPassword } from "@firebase/auth";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import auth from "@/lib/firebase";
import { useRouter } from "next/router";
import Button from "@/components/atoms/Button/Button";

const SignIn: React.FC = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [showPassword, setShowPassword] = useState(true);
  const [loading, setLoading] = useState(false);
  const router = useRouter();

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    setLoading(true);

    try {
      await signInWithEmailAndPassword(auth, email, password);
      console.log("User logged in successfully!");
      toast.success("Authenticated successfully!", {
        position: "top-center",
        autoClose: 3000,
        hideProgressBar: true,
      });

      router.push("/dashboard");
    } catch (error) {
      console.error("Error logging in:", error);
      toast.error("Failed to log in!", {
        position: "top-center",
        autoClose: 3000,
        hideProgressBar: true,
      });
    }

    setLoading(false);
  };

  const togglePasswordVisibility = () => {
    setShowPassword((prevShowPassword) => !prevShowPassword);
  };

  const handlePasswordFieldFocus = () => {
    setShowPassword(false);
  };

  const handlePasswordFieldBlur = () => {
    setShowPassword(true);
  };

  return (
    <div className="flex justify-center items-center h-screen">
      <form
        className="crystal w-96 rounded-lg p-8 space-y-6"
        onSubmit={handleSubmit}
      >
        <header className="flex mb-2 justify-center items-center">
          {showPassword ? (
            <Image
              src={PlaceholderImage1}
              alt="about image"
              width={100}
              height={100}
              className="rounded-full ring-2 ring-[#64748b]"
            />
          ) : (
            <Image
              src={PlaceholderImage2}
              alt="about image"
              width={100}
              height={100}
              className="rounded-full ring-2 ring-[#64748b]"
            />
          )}
        </header>
        <div>
          <label htmlFor="email" className="block font-medium">
            Email
          </label>
          <input
            type="email"
            id="email"
            className="mt-1 w-full border rounded-md border-ocean h-10 text-graphite p-5"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />
        </div>
        <div>
          <label htmlFor="password" className="block font-medium">
            Password
          </label>
          <div className="relative">
            <input
              type={showPassword ? "text" : "password"}
              id="password"
              className="mt-1 w-full border rounded-md border-ocean h-10 text-graphite p-5 pr-10"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              onFocus={handlePasswordFieldFocus}
              onBlur={handlePasswordFieldBlur}
              required
            />
            <button
              type="button"
              className="absolute top-1/2 right-2 transform -translate-y-1/2"
              onClick={togglePasswordVisibility}
            >
              {showPassword ? "🙈" : "👁️"}
            </button>
          </div>
        </div>
        <div className="flex justify-center">
          <Button className="w-full" variant="outline">
            {loading ? "Loading..." : "Log In"}
          </Button>
        </div>
        <ToastContainer />
      </form>
    </div>
  );
};

export default SignIn;