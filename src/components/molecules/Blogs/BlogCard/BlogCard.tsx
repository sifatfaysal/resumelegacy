import Link from "next/link";
import React from "react";
import Image, { StaticImageData } from "next/image";

interface BlogCardProps {
  title: string;
  date: string;
  excerpt: string;
  slug: string;
  image: StaticImageData;
}

const BlogCard: React.FC<BlogCardProps> = ({
  title,
  date,
  excerpt,
  slug,
  image,
}) => {
  return (
    <Link href={`/blog/${slug}`}>
      <div className="crystal">
        <Image
          className="rounded-t-lg w-full h-52 md:h-64 lg:h-72 object-cover"
          src={image}
          alt={title}
        />
        <div className="p-5 md:p-8">
          <h2 className="text-lg md:text-xl font-bold mb-2 truncate line-clamp-2">
            {title}
          </h2>
          <p className="text-gray-500 text-sm md:text-base mb-2 text-ocean">
            {date}
          </p>
          <p className="text-gray-700 text-sm md:text-base">{excerpt}</p>
          <p className="text-ocean py-2 md:py-4 hover:text-white">Read More</p>
        </div>
      </div>
    </Link>
  );
};

export default BlogCard;