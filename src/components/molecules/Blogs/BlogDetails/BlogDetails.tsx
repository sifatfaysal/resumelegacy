import React from "react";
import Image from "next/image";
import blogPosts from "../../../../data/blogPosts";
import { useRouter } from "next/router";
import CommentSection from "../../CommentSection/CommentSection";
import ReactMarkdown from "react-markdown";
import { AiOutlineCalendar, AiOutlineUser } from "react-icons/ai";
import { BiTimeFive } from "react-icons/bi";
import { SiGithubsponsors } from "react-icons/si";
import { TbBrandGithub } from "react-icons/tb";
import { BsFacebook, BsLinkedin } from "react-icons/bs";

function BlogDetails() {
  const router = useRouter();
  const slug = router?.query?.slug;
  const blogPost = blogPosts?.post?.find((post) => post?.slug === slug);
  console.log(slug);

  if (!blogPost) {
    return <div className="container">404 Not Found!</div>;
  }

  const { title, date, details, image, author } = blogPost;

  const recentViews = Array.from({ length: 4 }, (item, index) => (
    <li key={index}>
      <div className="crystal rounded-lg shadow-md p-4 flex">
        <div className="flex gap-4">
          <div>
            <p className="font-bold">{title}</p>
          </div>
        </div>
      </div>
    </li>
  ));

  return (
    <div className="container mx-auto">
      <div className="flex flex-wrap">
        <div className="w-full sm:w-3/4">
          <div className="p-4">
            <h1 className="text-[40px] font-bold my-5">{title}</h1>
            <div className="flex items-center gap-4 py-5">
              <div className="flex gap-2 items-center ">
                <AiOutlineCalendar className="w-6 h-6" /> {date}
              </div>
              <div className="flex gap-2 items-center">
                <AiOutlineUser className="w-6 h-6" /> {author}
              </div>
              <div className="flex gap-2 items-center">
                <BiTimeFive className="w-6 h-6" /> 2 Min Read
              </div>
            </div>
            <Image
              src={image}
              alt={title}
              width={1200}
              height={300}
              className="rounded-md"
            />
            <div className="py-[20px]">
              <p className="pb-4">Share</p>
              <div className="flex gap-2 md:gap-2 items-center mb-4 md:mb-0">
                <div className="crystal p-[10px]">
                  <div>
                    <SiGithubsponsors className="cursor-pointer hover:text-ocean" />
                  </div>
                </div>
                <div className="crystal p-[10px]">
                  <div>
                    <TbBrandGithub className="cursor-pointer hover:text-ocean" />
                  </div>
                </div>
                <div className="crystal p-[10px]">
                  <div>
                    <BsFacebook className="cursor-pointer hover:text-ocean" />
                  </div>
                </div>
                <div className="crystal p-[10px]">
                  <div>
                    <BsLinkedin className="cursor-pointer hover:text-ocean" />
                  </div>
                </div>
              </div>
            </div>
            <div className="text-gray-500 my-10">
              <ReactMarkdown>{details}</ReactMarkdown>
            </div>
            <CommentSection />
          </div>
        </div>
        <div className="w-full sm:w-1/4 md:w-1/4 hidden xl:block">
          <div className="p-4 my-16">
            <h2 className="text-lg font-bold mb-4">Recent Views</h2>
            <ul className="space-y-2">{recentViews}</ul>
          </div>
        </div>
      </div>
    </div>
  );
}

export default BlogDetails;