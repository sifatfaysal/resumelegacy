import React from "react";
import Image from "next/image";
import PlaceHolder from "@/static/images/placeholder.jpg";

interface BlogCardProps {
  title: string;
  excerpt: string;
  image: string;
  author: string;
}

const DailyBlogCard: React.FC<BlogCardProps> = ({ title, excerpt, author, image }) => {
  return (
    <div className="crystal rounded-md cursor-pointer">
      <div className="p-4 md:p-6">
        <div className="flex justify-between items-center">
          <div>
            <p className="text-ocean">#MicroBlog</p>
          </div>
          <div className="text-[#838181]">14 days ago</div>
        </div>

        <div className="pb-4">
          <h2 className="text-lg md:text-xl font-medium mb-2 mt-4">{title}</h2>
          <p className="text-[#AEAbAB] text-[16px] ">{excerpt}</p>
        </div>

        <div className="flex justify-between items-center">
          <div className="flex gap-2 items-center">
            <Image
              src={PlaceHolder}
              alt="about image"
              className="w-8 h-8 rounded-full ring-2 ring-ocean"
              width={32}
              height={32}
            />
            <p>{author}</p>
          </div>
          <div className="text-ocean">Read More</div>
        </div>
      </div>
    </div>
  );
};

export default DailyBlogCard;
