import { NextPage } from "next";
import projectdata from "@/data/postdata";
import React from "react";
import DailyBlogCard from "@/components/molecules/Blogs/DailyBlog/DailyBlogCard";
import Button from "@/components/atoms/Button/Button";

const DailyBlog: NextPage = () => {
  const handleButtonClick = () => {
    alert("Button clicked!");
  };

  return (
    <div
      id="noteworthyProjects"
      data-aos="fade-down"
      data-aos-easing="linear"
      data-aos-duration="1500"
    >
      <div className="container">
        <div className="py-8 md:py-16">
          <p className="text-sm md:text-base text-center leading-6 text-ocean">
            Dev Note
          </p>
          <p className="text-3xl md:text-4xl font-bold text-center">
            Daily Blog
          </p>
          <p className="text-sm md:text-base text-center leading-6 text-gray-500 py-4">
            Insights and updates on my daily coding adventures.
          </p>
          <div className="flex justify-center">
            <Button variant="outline" onClick={handleButtonClick}>
              Subscribe
            </Button>
          </div>
        </div>
      </div>
      <div className="container grid grid-cols-1 md:grid-cols-2 gap-5">
        {projectdata.map((post) => (
          <DailyBlogCard key={post.id} {...post} />
        ))}
      </div>
    </div>
  );
};

export default DailyBlog;
