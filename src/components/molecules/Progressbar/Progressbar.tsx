import React from "react";

interface ProgressBarProps {
  progress: number;
  title: string;
  progressCountTitle?: string;
}
const ProgressBar: React.FC<ProgressBarProps> = ({
  progress,
  title,
  progressCountTitle = "Progress",
}) => {
  return (
    <div className="w-full py-2 sm:py-4 px-2 sm:px-4">
      <div className="flex flex-col sm:flex-row justify-between items-center mb-2">
        <p className="text-gray-600 mb-2 sm:mb-0 sm:mr-2">{title}</p>
        <p className="text-gray-600">
          {progress} % {progressCountTitle}
        </p>
      </div>
      <div className="relative w-full h-2 rounded-full bg-gray">
        <div
          className="absolute top-0 left-0 h-full rounded-full bg-ocean"
          style={{ width: `${progress}%` }}
        />
      </div>
    </div>
  );
};

export default ProgressBar;