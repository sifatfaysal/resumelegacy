import React from "react";
import ProgressItems from "@/components/molecules/ProgressItems/ProgressItems";
import { BiCodeCurly } from "react-icons/bi";
import { FiServer } from "react-icons/fi";
import { TbPaint } from "react-icons/tb";
import { IoIosGitNetwork } from "react-icons/io";
import IconCarousel from "@/components/atoms/IconCarousel/IconCarousel";
import ProgressBar from "@/components/molecules/Progressbar/Progressbar";

const handleButtonClick = () => {
  console.log("Button clicked!");
};

const Skills = () => {
  const progress = [25, 50, 60, 65, 70, 75, 80, 85, 90, 95, 99, 100];
  return (
    <div
      className="container px-4 md:px-0"
      data-aos="fade-down"
      data-aos-easing="linear"
      data-aos-duration="1500"
    >
      <div className="flex justify-center items-center">
        <div className="py-8 md:py-12">
          <p className="text-[36px] md:text-[36px] font-bold text-center">
            Technologies
          </p>
          <p className="text-[16px] md:text-[16px] text-center leading-6 text-[#CCBEBE] py-[10px]">
            Here are some technologies I have been  <br /> working on
          </p>
        </div>
      </div>
      <IconCarousel />
      <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4 justify-items-center py-8 md:py-12 px-[150px]">
        <div className="w-full">
          <ProgressItems
            icon={<BiCodeCurly className="w-8 h-8 md:w-10 md:h-10" />}
            title="Frontend"
            subtitle="More than 4 years"
            content={
              <div className="mt-4">
                <ProgressBar
                  progress={progress[9]}
                  title="HTML5"
                  progressCountTitle=""
                />
                <ProgressBar
                  progress={progress[7]}
                  title="CSS"
                  progressCountTitle=""
                />
                <ProgressBar
                  progress={progress[2]}
                  title="Javascript"
                  progressCountTitle=""
                />
                <ProgressBar
                  progress={progress[6]}
                  title="React"
                  progressCountTitle=""
                />
              </div>
            }
          />
        </div>
        <div className="w-full">
          <ProgressItems
            icon={<FiServer className="w-8 h-8 md:w-10 md:h-10" />}
            title="Backend"
            subtitle="More than 2 years"
            content={
              <div className="mt-4">
                <ProgressBar
                  progress={progress[1]}
                  title="Node Js"
                  progressCountTitle=""
                />
                <ProgressBar
                  progress={progress[2]}
                  title="Firebase"
                  progressCountTitle=""
                />
                <ProgressBar
                  progress={progress[3]}
                  title="MySQL"
                  progressCountTitle=""
                />
                <ProgressBar
                  progress={progress[0]}
                  title="MongoDb"
                  progressCountTitle=""
                />
              </div>
            }
          />
        </div>
        <div className="w-full">
          <ProgressItems
            icon={<IoIosGitNetwork className="w-8 h-8 md:w-10 md:h-10" />}
            title="Tools"
            subtitle="More than 4 years"
            content={
              <div className="mt-4">
                <ProgressBar
                  progress={progress[9]}
                  title="Git"
                  progressCountTitle=""
                />
                <ProgressBar
                  progress={progress[9]}
                  title="Jira"
                  progressCountTitle=""
                />
                <ProgressBar
                  progress={progress[6]}
                  title="Cypress"
                  progressCountTitle=""
                />
                <ProgressBar
                  progress={progress[2]}
                  title="Docker"
                  progressCountTitle=""
                />
              </div>
            }
          />
        </div>
      </div>
    </div>
  );
};

export default Skills;
