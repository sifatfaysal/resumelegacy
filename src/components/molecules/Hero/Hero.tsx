import React, { useEffect } from "react";
import Link from "next/link";
import AOS from "aos";
import "aos/dist/aos.css";
import Button from "@/components/atoms/Button/Button";
import Image from "next/image";
import PlaceholderImage from "@/static/images/Sifat__Faysal__Main.png";

const Hero = () => {
  const handleButtonClick = () => {
    alert("Button clicked!");
  };

  useEffect(() => {
    AOS.init();
  }, []);

  return (
    <div
      className="container grid grid-cols-1 gap-4 sm:grid-cols-2 sm:justify-between my-10 sm:my-14"
      data-aos="fade-right"
    >
      <div className="flex flex-col justify-center items-center sm:items-start">
        <p className="text-4xl sm:text-6xl md:text-[55px] font-bold">
        Zahid Showrav <span className="text-ocean">.</span>
        </p>
        <p className="text-lg sm:text-xl md:text-2xl font-semibold text-ocean">
          Sr. Software Engineer
        </p>

        <p className="text-base sm:text-lg md:text-base font-light w-full sm:w-80 lg:w-96 py-5 text-center sm:text-left">
          I build accessible, inclusive products and digital experiences for the web.
        </p>

        <div className="flex flex-col sm:flex-row gap-4 my-5">
          <Button onClick={() => {}}>
            <Link
              href="https://drive.google.com/file/d/1qzUnV5YMpfGVguucqljjEDr2oE74qWR6/view?usp=share_link"
              target="_blank"
              rel="noopener noreferrer"
            >
              Get Resume
            </Link>
          </Button>
          <Button variant="outline">
            <Link href="/lightdesk">Work With Me</Link>
          </Button>
        </div>
      </div>

      <div className="flex flex-col justify-center items-center sm:flex-row sm:justify-end">
        <div className="w-48 sm:w-auto">
          {/*<Image
            src={PlaceholderImage}
            alt="about image"
            width={300}
            height={300}
            className="rounded-md"
          />*/}
        </div>
      </div>
    </div>
  );
};

export default Hero;
