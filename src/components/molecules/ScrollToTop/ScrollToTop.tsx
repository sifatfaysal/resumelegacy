import React, { useState, useEffect } from "react";
import { FaAngleUp } from "react-icons/fa";

const ScrollToTop = () => {
  const [showTopBtn, setShowTopBtn] = useState(false);

  useEffect(() => {
    const handleScroll = () => {
      if (window.scrollY > 200) {
        setShowTopBtn(true);
      } else {
        setShowTopBtn(false);
      }
    };
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  const goToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  return (
    <div className="scroll-to-top">
      {showTopBtn && (
        <div
          className="scroll-to-top-button cursor-pointer crystal p-[7px]"
          onClick={goToTop}
          aria-label="Scroll to top"
        >
          <FaAngleUp className="text-white w-6 h-6" />
        </div>
      )}
    </div>
  );
};

export default ScrollToTop;