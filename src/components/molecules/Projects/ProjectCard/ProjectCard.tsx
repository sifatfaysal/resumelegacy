import React from "react";
import Image from "next/image";
import { SiJavascript, SiMongodb, SiReact } from "react-icons/si";
import { TbBrandGithub, TbShare3 } from "react-icons/tb";

interface ProjectCardProps {
  title: string;
  date: string;
  excerpt: string;
  image: string;
}

const ProjectCard: React.FC<ProjectCardProps> = ({
  title,
  date,
  excerpt,
  image,
}) => {
  return (
    <div className="bg-gray rounded-md card cursor-pointer">
      <div className="relative">
        <Image
          className="rounded-t-md"
          src={image}
          alt={title}
          layout="responsive"
          width={500}
          height={300}
        />
      </div>
      <div className="p-4">
        <div className="py-2">
          <h2 className="text-lg md:text-xl font-medium mb-1">{title}</h2>
          <p className="text-gray-500 mb-1">{date}</p>
          <p className="text-gray-700">{excerpt}</p>
        </div>
        <div className="flex justify-between">
          <div className="flex gap-3 md:gap-5">
            <SiJavascript className="cursor-pointer" />
            <SiMongodb className="cursor-pointer" />
            <SiReact className="cursor-pointer" />
          </div>
          <div className="flex gap-3 md:gap-5">
            <TbBrandGithub className="cursor-pointer hover:text-ocean" />
            <TbShare3 className="cursor-pointer hover:text-ocean" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProjectCard;