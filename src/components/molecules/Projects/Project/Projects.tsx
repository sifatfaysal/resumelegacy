import { NextPage } from "next";
import projectdata from "@/data/projectdata";
import ProjectCard from "@/components/molecules/Projects/ProjectCard/ProjectCard";
import React from "react";

const Projects: NextPage = () => {
  return (
    <div id="work">
      <div className="container">
        <div className="">
          <p className="text-3xl md:text-4xl lg:text-5xl py-4 md:py-10 lg:py-20 text-center">
            Some Things I’ve Built
          </p>
        </div>
      </div>
      <div className="container grid grid-cols-1 sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-5">
        {projectdata.map((post) => (
          <ProjectCard key={post.id} {...post} />
        ))}
      </div>
    </div>
  );
};

export default Projects;
