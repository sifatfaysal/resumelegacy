import React, { useState } from "react";
import Image from "next/image";
import PlaceHolder from "@/static/images/project__Preview.png";
import { FiGithub } from "react-icons/fi";
import { TbBrandStackshare } from "react-icons/tb";

interface BlogCardProps {
  title: string;
  excerpt: string;
  image: string;
}

const NoteworthyProjectCard: React.FC<BlogCardProps> = ({
  title,
  excerpt,
  image,
}) => {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const handleCardClick = () => {
    setIsModalOpen(false);
  };

  const handleCloseModal = () => {
    console.log("Modal is being closed");
    setIsModalOpen(false);
  };

  const handleModalClick = (event: React.MouseEvent<HTMLDivElement>) => {
    event.stopPropagation();
  };

  return (
    <div
      className="crystal rounded-md cursor-pointer relative"
      onClick={handleCardClick}
    >
      <div className="p-2">
        <div className="pb-2">
          <div className="relative">
            <Image
              src={PlaceHolder}
              alt="about image"
              className="w-full rounded-t-md object-cover transition-all duration-500 grayscale hover:scale-95"
            />
            <div className="absolute inset-0 bg-black opacity-0 hover:opacity-50 transition-opacity duration-500"></div>
          </div>
          <h2 className="text-2xl font-medium mb-2 mt-2">{title}</h2>
          <p className="text-ocean">Flutter - MUI - Python - FastAPI</p>
          <p className="text-[#AEABAB]">{excerpt}</p>
        </div>
      </div>

      {isModalOpen && (
        <div
          className="fixed inset-0 flex items-center justify-center z-50 bg-black bg-opacity-50 backdrop-filter backdrop-blur-lg"
          onClick={handleCloseModal}
        >
          <div
            className="bg-[#1F2630] rounded-md w-[90%] h-[90%] sm:w-[600px] sm:h-[830px] border-2"
            onClick={handleModalClick}
          >
            <div>
              <Image
                src={PlaceHolder}
                alt="about image"
                className="w-full rounded-sm object-cover"
              />
            </div>

            <div className="p-4">
              <h2 className="text-2xl font-medium mb-2 mt-2">{title}</h2>
              <p className="text-ocean">Flutter - MUI - Python - FastAPI</p>
              <p className="text-[#AEABAB] py-2">
                Paint.app is a real-time coaching app for students learning to
                paint. This app is my baby, designed and built on my own. The
                tech stack is based on top of Flutter for the mobile app,
                connected to a Python & FastAPI backend, with data stored in
                Postgres, deployed on Heroku. Because this is real, here some
                gibberish to fill space Lorem ipsum dolor sit amet consectetur,
                adipisicing elit. Aspernatur quia officia odio nulla consectetur
                aperiam ad tempora magni magnam nesciunt. Fuga id sapiente
                facere ipsa eius exercitationem officiis deleniti, rerum
                dolorum. Deserunt soluta modi culpa animi.
              </p>
              <div>
                <p className="font-bold">
                  Project Links
                  <span className="text-ocean text-3xl pl-1">.</span>
                </p>
                <div className="flex gap-2 items-center">
                  <div className="flex gap-2 items-center">
                    <FiGithub />
                    <p className="hover:text-ocean hover:cursor-pointer">
                      Github
                    </p>
                  </div>

                  <div className="flex gap-2 items-center">
                    <TbBrandStackshare />
                    <p className="hover:text-ocean hover:cursor-pointer">
                      Live View
                    </p>
                  </div>
                </div>
              </div>
              <button
                className="mt-4 text-white crystal py-2 px-4"
                onClick={handleCloseModal}
              >
                Close
              </button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default NoteworthyProjectCard;
