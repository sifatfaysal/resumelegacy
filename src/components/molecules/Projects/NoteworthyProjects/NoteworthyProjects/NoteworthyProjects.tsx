import { NextPage } from "next";
import projectdata from "@/data/projectdata";
import React from "react";
import NoteworthyProjectCard from "@/components/molecules/Projects/NoteworthyProjects/NoteworthyProjectCard/NoteworthyProjectCard";

const NoteworthyProjects: NextPage = () => {
  return (
    <div
      id="noteworthyProjects"
      data-aos="fade-down"
      data-aos-easing="linear"
      data-aos-duration="1500"
    >
      <div className="container">
        <div className="py-4 md:py-8">
          <p className="text-base md:text-xl text-center leading-6 text-ocean">
            Showcase
          </p>
          <p className="text-[36px] md:text-[36px] font-bold text-center">
            Noteworthy Projects
          </p>
          <p className="text-[16px] md:text-[16px] text-center leading-6 text-[#CCBEBE] py-2 md:py-4 cursor-pointer hover:text-ocean hover:underline">
            view the archive
          </p>
        </div>
      </div>
      <div className="container grid grid-cols-1 md:grid-cols-3 gap-5">
        {projectdata.map((post) => (
          <NoteworthyProjectCard key={post.id} {...post} />
        ))}
      </div>
    </div>
  );
};

export default NoteworthyProjects;
