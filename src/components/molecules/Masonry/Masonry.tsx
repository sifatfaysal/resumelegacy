import React from "react";
import Image from "next/image";
import PlaceholderImage from "@/static/images/avatar.png";

const Masonry: React.FC = () => {
  return (
    <section
      data-aos="fade-down"
      data-aos-easing="linear"
      data-aos-duration="1500"
    >
      <div className="container">
        <div className="flex justify-center items-center pt-[50px]">
          <div className="py-8 md:py-12">
            <p className="text-[36px] md:text-[36px] font-bold text-center">
              Achievements
            </p>
            <p className="text-[16px] md:text-[16px] text-center leading-6 text-[#CCBEBE] py-[10px]">
              Here are some achievements I have earned during my journey in the  <br /> field of technology
            </p>
          </div>
        </div>

        <div className="flex gap-4 justify-center">
          <div className="relative max-w-xs overflow-hidden bg-cover bg-no-repeat cursor-pointer rounded-md crystal">
            <Image
              src={PlaceholderImage}
              alt="about image"
              className="rounded-md transition-transform transform-gpu group-hover:scale-105"
            />
            <div className="absolute bottom-0 left-0 right-0 top-0 h-full w-full overflow-hidden bg-ocean bg-fixed opacity-0 transition duration-300 ease-in-out hover:opacity-90">
              <div className="p-8 flex flex-col justify-end h-full">
                <p className="z-50 text-white text-[30px] font-bold">Hello</p>
                <p className="text-white">Hello Dev Today</p>
              </div>
            </div>
          </div>

          <div className="relative max-w-xs overflow-hidden bg-cover bg-no-repeat cursor-pointer rounded-md crystal">
            <Image
              src={PlaceholderImage}
              alt="about image"
              className="rounded-md transition-transform transform-gpu group-hover:scale-105"
            />
            <div className="absolute bottom-0 left-0 right-0 top-0 h-full w-full overflow-hidden bg-ocean bg-fixed opacity-0 transition duration-300 ease-in-out hover:opacity-90">
              <div className="p-8 flex flex-col justify-end h-full">
                <p className="z-50 text-white text-[30px] font-bold">Hello</p>
                <p className="text-white">Hello Dev Today</p>
              </div>
            </div>
          </div>

          <div className="relative max-w-xs overflow-hidden bg-cover bg-no-repeat cursor-pointer rounded-md crystal">
            <Image
              src={PlaceholderImage}
              alt="about image"
              className="rounded-md transition-transform transform-gpu group-hover:scale-105"
            />
            <div className="absolute bottom-0 left-0 right-0 top-0 h-full w-full overflow-hidden bg-ocean bg-fixed opacity-0 transition duration-300 ease-in-out hover:opacity-90">
              <div className="p-8 flex flex-col justify-end h-full">
                <p className="z-50 text-white text-[30px] font-bold">Hello</p>
                <p className="text-white">Hello Dev Today</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Masonry;
