import React from "react";
import { FaBriefcase, FaCoffee, FaClock, FaToolbox } from "react-icons/fa";

function FunFact() {
  return (
    <section
      data-aos="fade-down"
      data-aos-easing="linear"
      data-aos-duration="1500"
    >
      <div className="container py-8">
        <div className="grid grid-cols-2 gap-4 items-center">
          <div className="bg-gray-200 text-3xl sm:text-4xl md:text-5xl">
            <p className="font-bold">
              Fun Fact<span className="text-ocean text-6xl md:text-7xl">.</span>
            </p>
          </div>
        </div>

        <div className="grid grid-cols-2 gap-4 md:grid-cols-4">
          <div className="crystal p-5 rounded-lg shadow-lg">
            <div className="flex gap-8 items-center">
              <FaBriefcase className="text-4xl crystal p-1 text-ocean" />
              <p className="text-3xl font-bold">23</p>
            </div>
            <h3 className="text-2xl font-medium py-3">Projects Completed</h3>
          </div>

          <div className="crystal p-5 rounded-lg shadow-lg">
            <div className="flex gap-8 items-center">
              <FaCoffee className="text-4xl crystal p-1 text-ocean" />
              <p className="text-3xl font-bold">12</p>
            </div>
            <h3 className="text-2xl font-semibold py-3">Cups of Coffee</h3>
          </div>

          <div className="crystal p-5 rounded-lg shadow-lg">
            <div className="flex gap-8 items-center">
              <FaClock className="text-4xl crystal p-1  text-ocean" />
              <p className="text-3xl font-bold">38k</p>
            </div>
            <h3 className="text-2xl font-semibold py-3">Hour Worked</h3>
          </div>

          <div className="crystal p-5 rounded-lg shadow-lg">
            <div className="flex gap-8 items-center">
              <FaToolbox className="text-4xl crystal p-1 mb-2 text-ocean" />
              <p className="text-3xl font-bold">04</p>
            </div>
            <h3 className="text-2xl font-semibold py-3">Ongoing Task</h3>
          </div>
        </div>
      </div>
    </section>
  );
}

export default FunFact;