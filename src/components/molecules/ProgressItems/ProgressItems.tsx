import React, { useState, useEffect } from "react";
import { BiDownArrow, BiUpArrow } from "react-icons/bi";

interface ExpandableContentProps {
  icon: JSX.Element;
  title: string;
  subtitle: string;
  content: string | React.ReactNode;
}
function ProgressItems({
  icon,
  title,
  subtitle,
  content,
}: ExpandableContentProps) {
  const [expanded, setExpanded] = useState(true);

  const handleToggle = () => {
    setExpanded(!expanded);
  };

  useEffect(() => {
    const handleResize = () => {
      if (window.innerWidth < 768) {
        setExpanded(false);
      } else {
        setExpanded(true);
      }
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <>
      <div
        className="flex gap-5 justify-center items-center cursor-pointer"
        onClick={handleToggle}
      >
        <p>{icon}</p>
        <div>
          <p className="text-[20px]">{title}</p>
          <p className="text-[16px]">{subtitle}</p>
        </div>
        <p className={expanded ? "ArrowIcon down" : "ArrowIcon up"}>
          {expanded ? (
            <BiUpArrow className="cursor-pointer" />
          ) : (
            <BiDownArrow className="cursor-pointer" />
          )}
        </p>
      </div>
      {expanded && <div className="content">{content}</div>}
    </>
  );
}

export default ProgressItems;