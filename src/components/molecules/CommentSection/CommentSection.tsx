import React, { useState, useEffect } from "react";
import { v4 as uuidv4 } from "uuid";
import Image from "next/image";
import DevImage from "@/static/images/placeholder.jpg";
import Button from "@/components/atoms/Button/Button";

interface Comment {
  id: string;
  content: string;
  timestamp: string;
  replies: Comment[];
}

const CommentSection = (): JSX.Element => {
  const [comments, setComments] = useState<Comment[]>([]);
  const [newComment, setNewComment] = useState<string>("");
  const [showReplyInput, setShowReplyInput] = useState<string | null>(null);

  const addComment = (): void => {
    if (newComment.trim() !== "") {
      const comment: Comment = {
        id: uuidv4(),
        content: newComment,
        timestamp: new Date().toLocaleString(),
        replies: [],
      };

      setComments([...comments, comment]);
      setNewComment("");
    }
  };

  const addReply = (commentId: string, replyContent: string): void => {
    if (replyContent.trim() !== "") {
      const reply: Comment = {
        id: uuidv4(),
        content: replyContent,
        timestamp: new Date().toLocaleString(),
        replies: [],
      };

      const updatedComments = comments.map((comment) => {
        if (comment.id === commentId) {
          return {
            ...comment,
            replies: [...comment.replies, reply],
          };
        }
        return comment;
      });

      setComments(updatedComments);
    }
  };

  useEffect(() => {
    const storedComments = localStorage.getItem("comments");
    if (storedComments) {
      setComments(JSON.parse(storedComments));
    }
  }, []);

  useEffect(() => {
    localStorage.setItem("comments", JSON.stringify(comments));
  }, [comments]);

  const toggleReplyInput = (commentId: string): void => {
    setShowReplyInput(commentId === showReplyInput ? null : commentId);
  };

  return (
    <div>
      <div className="flex my-2 crystal p-[40px]">
        <input
          type="text"
          value={newComment}
          onChange={(e) => setNewComment(e.target.value)}
          placeholder="Write a comment..."
          className="w-full px-4 py-2 bg-graphite border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-blue-500"
        />
        <Button variant="outline" onClick={addComment} className="mx-[20px]">
          Post
        </Button>
      </div>

      <div className="crystal p-[40px]">
        {comments.map((comment) => (
          <div key={comment.id} className="flex items-start my-2">
            <div className="flex-shrink-0 mr-2">
              <Image
                src={DevImage}
                alt="about image"
                className="w-[50px] h-[50px] md:w-[60px] md:h-[60px] rounded-full ring-2 ring-ocean"
              />
            </div>
            <div>
              <div className="flex items-center mb-1">
                <span className="font-semibold">Anonimous User</span>
                <span className="text-gray-500 mx-2">•</span>
                <span className="text-gray-500">{comment.timestamp}</span>
              </div>
              <p>{comment.content}</p>
              <p
                className="text-ocean cursor-pointer"
                onClick={() => toggleReplyInput(comment.id)}
              >
                Reply
              </p>
              {showReplyInput === comment.id && (
                <div className="ml-8 mt-2 flex">
                  <input
                    type="text"
                    placeholder="Write a reply..."
                    className="w-full px-4 py-2  bg-graphite border border-gray-300 rounded-md focus:outline-none focus:ring focus:ring-blue-500"
                  />
                  <Button
                    className="mx-[20px]"
                    variant="outline"
                    onClick={() => addReply(comment.id, "Reply Content")}
                  >
                    Post
                  </Button>
                </div>
              )}

              <div className="ml-8 mt-2 py-[23px]">
                {comment.replies.map((reply) => (
                  <div
                    key={reply.id}
                    className="flex items-start mb-2 py-[20px]"
                  >
                    <div className="flex-shrink-0 mr-2">
                      <Image
                        src={DevImage}
                        alt="about image"
                        className="w-[40px] h-[40px] md:w-[50px] md:h-[50px] rounded-full ring-2 ring-ocean"
                      />
                    </div>
                    <div>
                      <div className="flex items-center mb-1">
                        <span className="font-semibold">User Name</span>
                        <span className="text-gray-500 mx-2">•</span>
                        <span className="text-gray-500">{reply.timestamp}</span>
                      </div>
                      <p>{reply.content}</p>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default CommentSection;