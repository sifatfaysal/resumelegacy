import React, { useState } from "react";
import Image from "next/image";
import Button from "@/components/atoms/Button/Button";
import Signature from "@/static/images/signature.png";
import PlaceholderImage from "@/static/images/avatar.png";
import {SiGithubsponsors} from "react-icons/si";
import {TbBrandGithub} from "react-icons/tb";
import {BsFacebook, BsLinkedin} from "react-icons/bs";
import Link from "next/link";

const AboutSection = () => {
  const [showFullText, setShowFullText] = useState(false);

  const handleTextButtonClick = () => {
    setShowFullText(!showFullText);
  };

  const handleButtonClick = () => {
    alert("Button Clicked!");
  };

  return (
      <section className="pb-16" id="about" data-aos="fade-down" data-aos-easing="linear" data-aos-duration="1500">
        <div className="container flex flex-col md:flex-row md:items-center gap-5">
          <div className="md:w-1/2 flex justify-center items-center">
            <div className="md:p-0 pb-16">
              <Image src={PlaceholderImage} alt="about image" width={300} height={300} className="rounded-md" />
            </div>
          </div>
          <div className="md:w-1/2 border-l-4 border-ocean pl-8">
            <div className="flex gap-2 md:gap-2 items-center mb-4 md:mb-0">
              <div className="crystal p-[10px]">
                <SiGithubsponsors className="cursor-pointer hover:text-ocean" />
              </div>
              <div className="crystal p-[10px]">
                <TbBrandGithub className="cursor-pointer hover:text-ocean" />
              </div>
              <div className="crystal p-[10px]">
                <BsFacebook className="cursor-pointer hover:text-ocean" />
              </div>
              <div className="crystal p-[10px]">
                <BsLinkedin className="cursor-pointer hover:text-ocean" />
              </div>
            </div>
            <h2 className="text-3xl md:text-4xl text-gray-dark font-bold mb-4">
              About Me <span className="text-ocean text-[70px]">.</span>
            </h2>
            {showFullText ? (
                <>
                  <p className="text-lg md:text-base text-gray-dark">
                    N/A
                  </p>
                  <p className="text-lg md:text-base text-ocean">
                    N/A
                  </p>
                </>
            ) : (
                <p className="text-lg md:text-base text-gray-dark">
                  I am Zahid Shawrav, a design-minded front-end software engineer focused on building beautiful interfaces & experiences
                </p>
            )}
            <div className="py-4">
              <Image src={Signature} alt="signature" className="w-[70px]" />
            </div>
            {/*<Button variant="outline" onClick={handleTextButtonClick}>
              {showFullText ? "Show Less" : "Read More"}
            </Button>*/}
          </div>
        </div>
      </section>
  );
};

export default AboutSection;
