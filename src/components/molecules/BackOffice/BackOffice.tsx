import React, { useState, useEffect } from "react";
import Button from "@/components/atoms/Button/Button";
import auth from "@/lib/firebase";
import { signOut } from "@firebase/auth";
import { useRouter } from "next/router";
import { useAuthentication } from "@/auth/auth";

const BackOffice: React.FC = () => {
  const router = useRouter();
  const [loading, setLoading] = useState(false);

  useAuthentication();

  useEffect(() => {
    if (!auth.currentUser) {
      router.replace("/login");
    }
  }, [router]);

  const handleLogout = async () => {
    setLoading(true);

    try {
      await new Promise((resolve) => setTimeout(resolve, 2000));

      await signOut(auth);
      console.log("User logged out successfully!");
      router.push("/login");
    } catch (error) {
      console.error("Error logging out:", error);
    }
    setLoading(false);
  };

  return (
    <div className="container py-14">
      <h1>Dashboard</h1>
      <Button variant="outline" onClick={handleLogout} disabled={loading}>
        {loading ? "Logging Out..." : "Logout"}
      </Button>
    </div>
  );
};

export default BackOffice;