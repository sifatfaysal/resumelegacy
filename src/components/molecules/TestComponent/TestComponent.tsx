import React from "react";
const TestComponent: React.FC = () => {
  return (
    <section className="container">
      <div className="flex justify-center items-center">Hello World!</div>
    </section>
  );
};

export default TestComponent;