import Image from "next/image";
import React from "react";
import PlaceHolder from "@/static/images/placeholder.jpg";

const Testimonials = () => {
  return (
    <div
      className="container pt-10 lg:pt-20"
      id="contact"
      data-aos="fade-down"
      data-aos-easing="linear"
      data-aos-duration="1500"
    >
      <div className="flex justify-center items-center">
        <div className="py-10">
          <p className="text-base md:text-xl text-center leading-6 text-ocean">
            Feedback
          </p>
          <p className="text-[36px] font-bold text-center">
            Testimonials
          </p>
        </div>
      </div>

      <div>
        <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6 md:gap-8">
          <div className="crystal p-6 md:p-8 shadow-md">
            <h2 className="text-lg md:text-xl font-bold mb-2">
              Md. Hasan
            </h2>
            <p className="text-[#AEABAB] text-sm md:text-base">
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry.
            </p>
            <div className="flex gap-2 md:gap-4 items-center pt-4">
              <div className="w-8 h-8 md:w-10 md:h-10">
                <Image
                  src={PlaceHolder}
                  alt="about image"
                  className="w-full h-full rounded-full ring-2 ring-ocean"
                />
              </div>
              <div>
                <p className="text-sm md:text-base font-medium">
                  Ideeza
                </p>
                <p className="text-xs md:text-sm">Dhaka, Bangladesh</p>
              </div>
            </div>
          </div>

          <div className="crystal p-6 md:p-8 shadow-md">
            <p className="text-lg md:text-xl font-bold mb-2">
              Md. Hussain
            </p>
            <p className="text-[#AEABAB] text-sm md:text-base">
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry.
            </p>
            <div className="flex gap-2 md:gap-4 items-center pt-4">
              <div className="w-8 h-8 md:w-10 md:h-10">
                <Image
                  src={PlaceHolder}
                  alt="about image"
                  className="w-full h-full rounded-full ring-2 ring-ocean"
                />
              </div>
              <div>
                <p className="text-sm md:text-base font-medium">
                  Ideeza
                </p>
                <p className="text-xs md:text-sm">Dhaka, Bangladesh</p>
              </div>
            </div>
          </div>

          <div className="crystal p-6 md:p-8 shadow-md">
            <h2 className="text-lg md:text-xl font-bold mb-2">
              Md. Ohi
            </h2>
            <p className="text-[#AEABAB] text-sm md:text-base">
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry.
            </p>
            <div className="flex gap-2 md:gap-4 items-center pt-4">
              <div className="w-8 h-8 md:w-10 md:h-10">
                <Image
                  src={PlaceHolder}
                  alt="about image"
                  className="w-full h-full rounded-full ring-2 ring-ocean"
                />
              </div>
              <div>
                <p className="text-sm md:text-base font-medium">
                  Ideeza
                </p>
                <p className="text-xs md:text-sm">Dhaka, Bangladesh</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Testimonials;
