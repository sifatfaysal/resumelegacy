import React from "react";
import Image from "next/image";
import MSL from "@/static/images/microsoft.png";
import GGL from "@/static/images/google.png";

const WorkLog: React.FC = () => {
  return (
    <div
      className="container"
      data-aos="fade-down"
      data-aos-easing="linear"
      data-aos-duration="1500"
      id="experiance"
    >
      <div className="pt-[50px]">
        <p className="text-[36px] text-center font-bold">Where I’ve Worked</p>
        <p className="text-[16px] md:text-[16px] text-center leading-6 text-[#CCBEBE] py-[10px]">
          Here are some of the companies I have <br />  worked with.
        </p>
      </div>

      <div className="grid grid-cols-2 gap-4 mt-24">
        <div className="relative mb-10 mr-6">
          <span className="absolute flex items-center justify-center w-6 h-6 bg-blue-100 rounded-full -left-3 -top-4 ring-8 ring-white">
            <Image
              src={MSL}
              alt="about image"
              width={300}
              height={300}
              className="rounded-md"
            />
          </span>
          <div className="p-4 crystal border border-gray-200 rounded-lg shadow-sm">
            <div className="items-center justify-between mb-3 sm:flex">
              <time className="mb-1 text-xs font-normal text-gray-400 sm:order-last sm:mb-0">
                2019
              </time>
              <div className="">
                <p className="text-[20px] font-bold text-gray-500">
                JoulesLabs
                </p>
                <p>Software Engineer</p>
              </div>
            </div>
            <div className="p-3 crystal text-xs italic font-normal text-gray-500 border border-gray-200 rounded-lg bg-gray-50">
              In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content.
            </div>
          </div>
        </div>

        <div className="relative mb-10 mr-6">
          <span className="absolute flex items-center justify-center w-6 h-6 bg-blue-100 rounded-full -left-3 -top-4 ring-8 ring-white">
            <Image
              src={GGL}
              alt="about image"
              width={300}
              height={300}
              className="rounded-md"
            />
          </span>
          <div className="p-4 crystal border border-gray-200 rounded-lg shadow-sm">
            <div className="items-center justify-between mb-3 sm:flex">
              <time className="mb-1 text-xs font-normal text-gray-400 sm:order-last sm:mb-0">
                2021
              </time>
              <div className="">
                <p className="text-[20px] font-bold text-gray-500">
                  Ideeza
                </p>
                <p>Software Engineer (SQA)</p>
              </div>
            </div>
            <div className="p-3 crystal text-xs italic font-normal text-gray-500 border border-gray-200 rounded-lg bg-gray-50">
              In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content.
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default WorkLog;
