import SignIn from "@/components/molecules/login/SignIn";
import React from "react";

const Login: React.FC = () => {
  return (
    <div>
      <SignIn />
    </div>
  );
};

export default Login;