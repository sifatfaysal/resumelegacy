import { NextPage } from "next";
import Layout from "@/components/layouts/layout";
import React from "react";
import BlogDetails from "@/components/molecules/Blogs/BlogDetails/BlogDetails";

const BlogSlug: NextPage = () => {
  return (
    <Layout>
      <BlogDetails />
    </Layout>
  );
};

export default BlogSlug;