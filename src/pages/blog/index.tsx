import React from "react";
import { NextPage } from "next";
import blogPosts from "@/data/blogPosts";
import Layout from "@/components/layouts/layout";
import BlogCard from "@/components/molecules/Blogs/BlogCard/BlogCard";
import { BiCategoryAlt, BiRightArrow } from "react-icons/bi";
import SearchField from "@/components/atoms/SearchPost/Search";
import Link from "next/link";

const BlogPage: NextPage = () => {
  return (
    <Layout>
      <div className="container py-10 md:py-10">
        <div className="flex flex-col md:flex-row justify-between items-center py-5 md:py-10 px-[25px]">
          <div className="w-full md:w-1/2">
            <p className="text-xl md:text-2xl font-bold">
              Daily Blogs<span className="text-ocean text-[50px]">.</span>
            </p>
          </div>
          <div className="w-full md:w-1/2 flex justify-end">
            <SearchField />
          </div>
        </div>

        <div className="grid grid-cols-2 md:grid-cols-4 gap-4 px-[25px]">
          {blogPosts.categories.map((category) => {
            const totalPosts =
              blogPosts?.post?.filter(
                (b) => b?.category_slug === category?.slug,
              )?.length ?? 0;
            return (
              <div key={category.name}>
                <Link href={`/blog/category/${category?.slug}`}>
                  <div className="bg-gray-100 card rounded-md p-2 md:p-5 cursor-pointer crystal">
                    <div className="flex flex-col md:flex-row justify-between items-center">
                      <div className="flex gap-4 md:gap-6 items-start flex-wrap">
                        <BiCategoryAlt className="w-10 md:w-14 h-10 md:h-14 text-ocean" />
                        <div>
                          <p className="text-lg md:text-xl font-medium">
                            {category.name}
                          </p>
                          <p className="text-base md:text-lg">
                            {totalPosts} Post
                          </p>
                        </div>
                      </div>
                      <div className="hidden">
                        <BiRightArrow className="w-6 md:w-8 h-6 md:h-8" />
                      </div>
                    </div>
                  </div>
                </Link>
              </div>
            );
          })}
        </div>

        <div className="container grid grid-cols-1 md:grid-cols-3 gap-4 py-10 md:py-20">
          {blogPosts?.post?.map((post) => (
            <BlogCard key={post?.slug} {...post} />
          ))}
        </div>
      </div>
    </Layout>
  );
};

export default BlogPage;
