import { NextPage } from "next";
import Layout from "@/components/layouts/layout";
import React from "react";
import { useRouter } from "next/router";
import blogPosts from "@/data/blogPosts";
import BlogCard from "@/components/molecules/Blogs/BlogCard/BlogCard";
import Button from "@/components/atoms/Button/Button";
import Image from "next/image";

const CategoryBlogs: NextPage = () => {
  const router = useRouter();
  const slug = router?.query?.slug;
  const category = blogPosts?.categories?.find((c) => c?.slug === slug);
  const categoryBlogs = blogPosts?.post?.filter(
    (post) => post?.category_slug === slug,
  );

  if (!categoryBlogs) {
    return <div className="container">404 Not Found!</div>;
  }

  const handleButtonClick = () => {
    alert("Button clicked!");
  };

  const categoryIcon = category?.icon;

  return (
    <Layout>
      <div className="container py-10 md:py-20">
        <div className="flex flex-col md:flex-row gap-4">
          <div className="md:w-1/2">
            <p className="text-4xl md:text-6xl text-white font-bold leading-tight mb-4 md:mb-8">
              {category?.name}
            </p>
            <p className="text-lg md:text-xl mb-6">{category?.description}</p>
            <div className="pb-10 md:pb-0">
              <Button variant="outline" onClick={handleButtonClick}>
                Learn More
              </Button>
            </div>
          </div>
          <div className="md:w-1/2 flex justify-end items-center">
            <div className="w-full md:max-w-md lg:max-w-lg hidden md:flex justify-end">
              <Image
                src={categoryIcon ? categoryIcon : ""}
                alt="category image"
                width={200}
                height={200}
                className="rounded-md"
                unoptimized={true}
              />
            </div>
          </div>
        </div>
      </div>
      <div className="container grid grid-cols-1 md:grid-cols-3 gap-8 md:gap-12 py-10 md:py-20">
        {categoryBlogs?.map((post) => (
          <BlogCard key={post?.slug} {...post} />
        ))}
      </div>
    </Layout>
  );
};

export default CategoryBlogs;