import React from "react";
import Layout from "@/components/layouts/layout";
import BackOffice from "@/components/molecules/BackOffice/BackOffice";
const Dashboard: React.FC = () => {
  return (
    <Layout>
      <BackOffice />
    </Layout>
  );
};

export default Dashboard;