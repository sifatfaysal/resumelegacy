  import React from "react";
import Layout from "@/components/layouts/layout";
import Hero from "@/components/molecules/Hero/Hero";
import AboutSection from "@/components/molecules/About/About";
import Skills from "@/components/molecules/Skills/Skills";
import NoteworthyProjects from "@/components/molecules/Projects/NoteworthyProjects/NoteworthyProjects/NoteworthyProjects";
import GetInTouch from "@/components/molecules/GetinTouch/GetinTouch";
import DailyBlog from "@/components/molecules/Blogs/DailyBlog/DailyBlog";
import Testimonials from "@/components/molecules/Teastimonials/Testimonials";
import WorkLog from "@/components/molecules/Timeline/WorkLog";
import FunFact from "@/components/molecules/FunFact/FunFact";
import Masonry from "@/components/molecules/Masonry/Masonry";

export default function Home() {
  return (
    <Layout>
      <Hero />
      <AboutSection/>
      <WorkLog />
      <Skills />
      <NoteworthyProjects />
      {/*<FunFact />*/}
      <Masonry />
      <DailyBlog />
      <Testimonials />
      <GetInTouch />
    </Layout>
  );
}
