import Head from "next/head";
import "@/static/globalcss/tailwind.css";
import "@/static/globalcss/fonts.css";
import "@/static/globalcss/global.css";
import type {AppProps} from "next/app";
import {ThemeProvider, useTheme} from "@/context/ThemeContext";
import CookieConsent from "@/components/atoms/CookieConsent/CookieConsent";
import NextNProgress from 'nextjs-progressbar';
import {DevSupport} from "@react-buddy/ide-toolbox-next";
import {ComponentPreviews, useInitial} from "@/dev";

function MyApp({Component, pageProps}: AppProps) {
    const {darkMode} = useTheme();

    return (
        <>
            <ThemeProvider>
                <div className={` ${darkMode ? "dark" : ""}`}>
                    <Head>
                        <title>Zahid | Showrav</title>
                    </Head>
                    <NextNProgress/>
                    <DevSupport ComponentPreviews={ComponentPreviews}
                                useInitialHook={useInitial}
                    >
                        <Component {...pageProps} />
                    </DevSupport>
                    <CookieConsent/>
                </div>
            </ThemeProvider>
        </>
    );
}

export default MyApp;
