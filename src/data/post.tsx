import PlaceholderImage from "@/static/images/project__Preview.png";

const posts = {
  categories: [
    {
      name: "Astronomy",
      slug: "astronomy",
      description:
        "Lorem ipsum dolor sit amet, consecrate disciplining elite . . ",
      icon: PlaceholderImage,
    },
    {
      name: "Programming",
      slug: "programming",
      description: "Lorem ipsum dolor sit amet, consecrate disciplining elite",
      icon: PlaceholderImage,
    },
    {
      name: "CSS Tricks",
      slug: "css-tricks",
      description: "Lorem ipsum dolor sit amet, consecrate disciplining elite",
      icon: PlaceholderImage,
    },
    {
      name: "Math Solution",
      slug: "math-solution",
      description: "Lorem ipsum dolor sit amet, consecrate disciplining elite",
      icon: PlaceholderImage,
    },
  ],
  post: [
    {
      id: 1,
      title: "NaN === “number”",
      date: "April 1, 2023",
      author: "Sifat Faysal",
      excerpt: "Lorem ipsum dolor sit amet, consecrate disciplining elite...",
      slug: "my-first-blog-post1",
      category_slug: "astronomy",
      image: PlaceholderImage,
      details: `
## Introduction
In this blog post, we will explore the concept of NaN (Not a Number) and its comparison with the data type "number."

### What is NaN?
NaN is a special value in JavaScript that represents "Not a Number." It is typically the result of an invalid or undefined mathematical operation.

### Comparing NaN with "number"
When we compare NaN with the data type "number" using the triple equals (===) operator, the result may surprise you.
Let's see an example:

\`\`\`js
console.log(NaN === "number"); // Output: false
\`\`\`

### Conclusion
NaN is not equal to the data type "number" due to its unique nature as a special value.
Feel free to explore more about NaN and its behavior in JavaScript!
`,
    },
    {
      id: 2,
      title: "Why do we use it?",
      date: "April 1, 2023",
      author: "Sifat Faysal",
      excerpt: "Lorem ipsum dolor sit amet, consecrate disciplining elite...",
      slug: "my-first-blog-post2",
      category_slug: "programming",
      image: PlaceholderImage,
      details: `
## Introduction
In this blog post, we will discuss the importance of using Markdown in web applications.

### What is Markdown?
Markdown is a lightweight markup language that allows you to format text using a simple syntax. It is widely used for creating content on the web, including blog posts, documentation, and more.

### Advantages of Markdown
- Easy to learn and use: Markdown uses a straightforward syntax that is quick to pick up and use.
- Platform-independent: Markdown can be used on any platform and in any text editor.
- Readable source code: Markdown source files are human-readable, making it easy to collaborate with others.
- HTML compatibility: Markdown can be converted into HTML, making it ideal for web content.

### Conclusion
Markdown is a powerful tool for creating content on the web. Its simplicity and versatility make it a popular choice for developers, writers, and content creators.

Start using Markdown in your web applications to enhance the content creation experience!
`,
    },
    {
      id: 3,
      title: "Where does it come from?",
      date: "April 1, 2023",
      author: "Sifat Faysal",
      excerpt: "Lorem ipsum dolor sit amet, consecrate disciplining elite...",
      slug: "my-first-blog-post3",
      category_slug: "programming",
      image: PlaceholderImage,
      details: `
## Introduction
In this blog post, we will explore the origins of Markdown and its creator.

### Markdown's Creator
Markdown was created by John Gruber in 2004. It was designed as a simple way to format text for the web using a plain text syntax. John Gruber's goal was to create a tool that would be easy to use and read, while also being compatible with HTML.

### Evolution of Markdown
Over the years, Markdown has evolved and gained popularity among developers, writers, and content creators. It has become the go-to format for creating content on platforms like GitHub, Stack Overflow, and more.

### Conclusion
Markdown's simplicity and versatility have made it a widely adopted format for web content creation. Thanks to John Gruber's vision and effort, Markdown continues to be a valuable tool for developers and content creators worldwide.

Explore the world of Markdown and enhance your content creation experience!
`,
    },
  ],
};

export default posts;