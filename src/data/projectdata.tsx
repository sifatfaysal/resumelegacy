const projectdata = [
  {
    id: 1,
    title: "Paint.app",
    date: "April 1, 2023",
    excerpt: "Lorem ipsum dolor sit amet, consecrate disciplining elite...",
    image:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRomA95BWGhUESf6_Tj-jUHtERZlx3bSAmiWJtYVV-7Vcq3inZLaM7XT7x_bufr2lZ6-Cw&usqp=CAU",
  },
  {
    id: 2,
    title: "BrushWire",
    date: "April 1, 2023",
    excerpt: "Lorem ipsum dolor sit amet, consecrate disciplining elite...",
    image:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRomA95BWGhUESf6_Tj-jUHtERZlx3bSAmiWJtYVV-7Vcq3inZLaM7XT7x_bufr2lZ6-Cw&usqp=CAU",
  },
  {
    id: 3,
    title: "The Canvas Club",
    date: "April 1, 2023",
    excerpt: "Lorem ipsum dolor sit amet, consecrate disciplining elite...",
    image:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRomA95BWGhUESf6_Tj-jUHtERZlx3bSAmiWJtYVV-7Vcq3inZLaM7XT7x_bufr2lZ6-Cw&usqp=CAU",
  },
];

export default projectdata;