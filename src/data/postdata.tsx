const Postdata = [
  {
    id: 1,
    title: "Avoid Memory Leaks and Race Conditions in API Calls",
    date: "April 1, 2023",
    author: "Zahid Showrav",
    excerpt: "Are you tired of dealing with memory leaks and race conditions in your front-end applications when making API calls ...",
    image:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRomA95BWGhUESf6_Tj-jUHtERZlx3bSAmiWJtYVV-7Vcq3inZLaM7XT7x_bufr2lZ6-Cw&usqp=CAU",
  },
  {
    id: 3,
    title: "Shifting Frontend to the Server",
    date: "April 1, 2023",
    author: "Zahid Showrav",
    excerpt: "Are you tired of dealing with memory leaks and race conditions in your front-end applications when making API calls ...",
    image:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRomA95BWGhUESf6_Tj-jUHtERZlx3bSAmiWJtYVV-7Vcq3inZLaM7XT7x_bufr2lZ6-Cw&usqp=CAU",
  },
];

export default Postdata;
