import { useEffect } from "react";
import { useRouter } from "next/router";
import { onAuthStateChanged } from "@firebase/auth";
import auth from "@/lib/firebase";

export const useAuthentication = () => {
  const router = useRouter();

  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, (user) => {
      if (!user) {
        router.replace("/login");
      }
    });

    return () => unsubscribe();
  }, [router]);
};